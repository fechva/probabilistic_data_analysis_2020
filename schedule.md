# Probabilistic data analysis: research seminar 2020

## Schedule

The following links list study material for each session (available one week in advance for each session).

[Session 1](session1.md) (March 20, 2020 / Leo Lahti)

  * Introduction (Chapters 1-2)  
     
[Session 2](session2.md) (April 3, 2020 / Leo Lahti & Ville Laitinen)

  * Priors (Chapters 2-3)
  * rstanarm R package (Related to chapters 1-3)  
     
[Session 3](session3.md) (April 17, 2020 / Aaro Salosensaari & Felix Vaura)

  * Posterior approximations (Chapter 4.1-4.4)
  * Relation to other methods (Chapter 4.5+)  
     
[Session 4](session4.md) (May 1, 2020)

  * Hierarchical models (Chapter 5)
  * BRMS R package (Related to chapter 5)  
     
[Session 5](session5.md) (May 15, 2020)

  * Model checking (Chapter 6)
  * Model comparison (Chapter 7)       

[Session 6](session6.md) (May 29, 2020)

  * Feature selection (Chapter 7)
  * Decision analysis (Chapter 9)       

[Session 7](session7.md) (June 12, 2020)

  * Bayesian computation (Chapter 10)
  * Approximations & samplng (Chapter 11)      

[Session 8](session8.md) (June 26, 2020)

  * Hamiltonian Monte Carlo and related (Chapter 12)
  * Stan (In more detail)  


## Participation

Each participant will:

  1. study the material and do the exercises in advance before every session
  1. participate actively in the sessions, and
  1. in his/her turn choose and announce study material one week before the next session

For other practical information, see [README](README.md) (time, location, contact details).

## Source material

To prepare a session on your own turn, check the following [material](material.md).

