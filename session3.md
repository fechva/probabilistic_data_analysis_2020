# Research seminar on probabilistic data analysis: Session 2



# Discussion topic 1 (Aaro Salosensaari): Laplace and other approximations

 * Chapters 4.1-4.4
 * Possibly other material (TBA by Fri, April 10)


# Discussion topic 2 (Felix Vaura): Relation to other statistical methods

 * Chapters 4.5
 * Possibly other material (TBA by Fri, April 10)


### Exercises

Some code for the discussed assignments [here](exercises/).

